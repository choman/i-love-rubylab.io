=== redo

There is another thing called `redo`. `next` skips any execution further and the iterating variable is incremented / decremented to next possible valuefootnote:[Its not right to think that next will increment iterating value by 1. Checkout link:code/next_with_step.rb[next_with_step.rb] and try it.], `redo` on other hands skips further execution of code in the loop block, but the iterating variable is not incremented, instead the loop is rerun. Type the code below and execute it

[source, ruby]
----
include::code/redo.rb[]
----

Run it and hopefully you can explain it by yourself ;-)

