=== downto
In your text editor type the following program

[source, ruby]
----
include::code/count_down_1.rb[]
----

Run it and see. Well your program uses now a lot less code and yet it produces the same result!

Notice the thing `10.downto 1` , this statement make Zigor count down from 10 to 1 , while it count downs you can do some thing with the countdown value, you can put some code in the loop block. The loop starts with a `do` and ends when it encounters a `end` command. Any code you put should be between the `do` and end `block` footnote:[You can use open and closed flower or curly brackets `{` and `}` instead of do and end in Ruby] as shown below

[source, ruby]
----
10.downto 1 do
  # do some thing! Anything!!
end
----

So between the `do` and `end`  (technically its called a block) you can put the code to print the count down number. First how to get the number? we will get it in a variable called `num` , so we rewrite the code as shown

[source, ruby]
----
10.downto 1 do |num|
  # put the printing stuff here
end
----

Notice above that `num` is surrounded by `|` and `|` . All we need to do now is to print it, so we just print it as shown below

[source, ruby]
----
10.downto 1 do |num|
  p num
end
----

