== Factory Pattern

Imagine a restaurant, its actually a food factory. If you need dosafootnote:[https://en.wikipedia.org/wiki/Dosa] you ask the waiter for it, if you need idiyappamfootnote:[https://en.wikipedia.org/wiki/Idiyappam] you ask the waiter for it. In  essence the restaurant or the food factory has created a common interface called the waiter for you to order anything. You just ask him, he deliveres and you need not care about how dosa is made or how idiyappam is made.

In programming you can do the same thing, you can implement a factory class that hides the difficulties of manufacturing an object and provides you with a common interface to make objects. Take a look at the code link:code/design_patterns/factory.rb[factory.rb] below. Type and run it.

[source, ruby, linenums]
----
include::code/design_patterns/factory.rb[]
----

Output:

[source]
----
In instance of Square
In instance of Circle
----

Lets see how it works. In the code the highlight is this one

[source, ruby]
----
class ShapeFactory
  def get_shape type
     case type
     when :square then Square.new
     when :circle then Circle.new
     end
  end
end
----

We have a class named `ShapeFactory` which provides a common interface to make objects via the function `get_shape`. To this `get_shape` we pass what type of object we want it to create and it creates it. Since this is just an example, the factory pattern here looks like its making the code complicated than simpler to the programmer, but in real life creation of an object could be very complex, and if factory classes can hide the complexity, then the life of programmer using it to build his software will become simple.

=== Exercise

Imagine you are writing a software for a game and you need to create enemy objects like tank, helicopter, missile launcher, infantry and so on. Write a factory pattern where when you call `Enemy.random` returns a random enemy object. 

