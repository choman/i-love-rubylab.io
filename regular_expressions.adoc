== Regular Expressions

You are with your girl friend in a jewel shop, she chooses one of the finest diamond rings and gives you THAT LOOK... Sure you know that you will fall into more credit card debt. You are in another day and having a talk with your boss to get more salary when he stares at you. THAT LOOK. You now know that what ever you say that you did good to the company, it will be ignored and would be futile. We all see for expressions in others face and we try to predict what's next from it.

Lets say that you are on chat with your friend and he types :-) , you know he is happy, this :-( means he's sad. So its quiet evident that we can see expressions even in textual data just as we see in each others face. Ruby's regular expression provides you with a way to detect these patterns in a given text and extract them if you wish. This can be used for some thing useful. This chapter is about that. What Ruby does not do is to tell you how to impress your girl without getting into debt :-( . Report this as a bug and hope they will fix it in ruby's next major release  ;-)

Fire up your irb, lets begin.

=== Creating an empty regexp

Okay we will try to create a empty regular expression. In your terminal type `irb -–simple-prompt` and in it type the following (without the `>>`, which is irb's prompt)

[source, ruby]
----
>> //.class
=> Regexp
----
You see `//.class` is `Regexp`. In other words any thing between those `/` and `/` is a regexpfootnote:[Will be using regexp instead of Regular expression from now on]. Regexp is not a string, but it denotes a pattern. Any string can match or may not match the pattern.

=== Detecting Patterns

Lets now see how to detect patterns in a regular expressions. Lets say that you want to see whether `abc` is in a given string. Just punch the code below

[source, ruby]
----
>> /abc/.match "This string contains abc"
=> #<MatchData "abc">
----

`abc` is present in the given string hence you get a match. In the code snippet below, there is no `abc` in the string and hence it returns `nil`.

[source, ruby]
----
>> /abc/.match "This string contains cba"
=> nil
----

You can use the `match` function on a regexp as shown above or you can use it on a string, as shown below. Both gives you the same result.

[source, ruby]
----
>> "This string contains abc".match(/abc/)
=> #<MatchData "abc">
----

Another way of matching is shown below. You can use the `=~` operator.

[source, ruby]
----
>> "This string contains abc" =~ /abc/
=> 21
>> /abc/ =~ "This string doesn't have abc"
=> 25
----
The `=~` tells you the location where the first occurrence of the match occurs.

If you would like something much simple, that is if you just want to know if a match is present or not, then you can use `match?`` Which returns `true` or `false` as shown below

[source, ruby]
----
>> /abc/.match? "This string contains abc"
=> true
>> /abc/.match? "This string contains def"
=> false
----

==== Things to remember

There are some things you need to remember, or at least refer from time to time. Those are mentioned in table belowfootnote:[I got this list from http://rubular.com/].

:===

Thing : What it means
.
Any single character

\w
Any word character (letter, number, underscore)

\W
Any non-word character

\d
Any digit

\D
Any non-digit

\s
Any whitespace character

\S
Any non-whitespace character

\b
Any word boundary character

^
Start of line

$
End of line

\A
Start of string

\z
End of string

[abc]
A single character of: a, b or c

[^abc]
Any single character except: a, b, or c

[a-z]
Any single character in the range a-z

[a-zA-Z]
Any single character in the range a-z or A-Z

(...)
Capture everything enclosed

(a|b)
a or b

a?
Zero or one of a

a*
Zero or more of a

a+
One or more of a

a{3}
Exactly 3 of a

a{3,}
3 or more of a

a{3,6}
Between 3 and 6 of a

i
case insensitive

m
make dot match newlines

x
ignore whitespace in regex

o
perform #{...} substitutions only once

:===

Don't panic if you don't understand it, you will catch up.

=== The dot

The dot in a regular expression matches any thing. To illustrate it lets try some examples in our irb

[source, ruby]
----
>> /.at/.match "There is rat in my house"
=> #<MatchData "rat">
----

in the above code snippet, we try to match `/.at/` with a string, and it matches. The reason, the string contains a word called `rat`. Take a look at another two examples below, the `/.at/` matches `cat` and `bat` without any fuss.

[source, ruby]
----
>> /.at/.match "There is cat in my house"
=> #<MatchData "cat">
>> /.at/.match "There is bat in my house"
=> #<MatchData "bat">
----

Its not a rule that the dot should be at the start of the word or something, it can be any where. A regexp `/f.y/` could comfortably match `fry` and `fly`. Ah! I wish I have a chicken fry now. Any way chickens do fly.

=== Character classes

Lets say tat we want to find that weather there is a bat or a rat or a cat is present in a given string. If its there we will print that there is a animal in the house, else we won't print a thing. You might be thinking that we need to have three regexp like `/bat/`, `/cat/` and `/rat/`, but that's not the case. We know from those three regexp that only the first character varies. So what about `/.at/` like the previous one. Well that won't work either, because it will match words like eat, mat, fat and so on.

So this time strictly we want to match only bat, rat and cat, so we come up with a regexp like this: `/[bcr]at/`, this will only match those three animal words and nothing else. Punch in the following example and run it in your computer

[source, ruby]
----
include::code/regexp_character_classes.rb[]
----

Output

----
There is a animal in your house
There is a animal in your house
There is a animal in your house
----

As you can see from the output above, the string “There is a animal in your house” gets printed thrice and not the fourth time where the match fails for "There is mat in my house".

Character classes can also accept ranges. Punch in the code below and run it.

[source, ruby]
----
include::code/regexp_character_classes_1.rb[]
----

Output

----
Enter a short string: fly
The string contains character(s) from a to z
The string contains character(s) other than A to Z
The string contains number(s) other than 0 to 9
The string contains characters other than vowels
----

Output 1

----
Enter a short string: Burgundy 32
The string contains character(s) from a to z
The string contains character(s) from A to Z
The string contains number(s) from 0 to 9
The string contains vowels
The string contains character(s) other than a to z
The string contains character(s) other than A to Z
The string contains number(s) other than 0 to 9
The string contains characters other than vowels
----

OK, what you infer from the output? When you give fly these regexp's match:

* /[a-z]/ since it contains a character from a to z
* /[^A-Z]/ since it contains a character that does not belong any where from A to Z, hence you come to know ^ inside a capture means negation. There are also other uses for ^ which if I am not lazy you will be writing about it.
* /[^0-9]/ since it does not contain any numbers from 0 to 9
* /[^aeiou]/ since it does not contain a vowel (a or e or i or o or u)

According to that the messages in the `puts` statements gets printed.
Now look at the Output 1, I have given the program a string `Burgundy 27`, check if your assumptions / logic tally with it.

=== Scanning

I love this `scan` method in String Class. It lets us search a huge array of string for something. Just like needle in a haystack, since computers are getting faster and faster you can scan more and more. They are good for searching. They are quiet unlike the police in India who would only conduct a search only if the person who has been burgled gives bribe.

So punch in the program below. It scans for words in a string.

[source, ruby]
----
include::code/regexp_scan.rb[]
----

Output

----
The words are:
["There", "are", "some", "words", "in", "this", "string", "and", "this", "program", "will", "scan", "those", "words", "and", "tell", "their", "word", "count"]
there are 19 words in the string
there are 16 unique words in string
----

Note the `/\w+/`,  what does it mean? Refer this table <<Things to remember>>. You can see that `\w` means any character like letter, number, underscore and `+` mean one or many. In other words I have assumed that words consists of any letter, number and underscore combinations and a word contains at-least one letter or more. So the statement `string.scan(/\w+/)` will scan all words and put into into a variable called words which we use in this program.

The statement `p words` prints out the array, and in the following line

[source, ruby]
----
puts "there are #{words.count} words in the string"
----

we are counting the number of elements in the array words using the command `word.count` and embedding in a string using `#{words.count}`  and printing it out to the user.

In the next statement

[source, ruby]
----
puts "there are #{words.uniq.count} unique words in string"
----

we are finding how many unique words are there in the given string using `words.uniq.count` and printing that too to the user. For example if you scan a large book of two authors and feed it to this program, the person who has more number of unique words can be assumed to have better vocabulary.

Lets now see another program. For example take a tweet you will do on twitter and you want to find out if the tweet contains twitter user names. So now lets analyze construct of a twitter user name, it first contains an @ symbol followed by a word. In other  words it must match the regexp `/@\w+/`. In the following program we scan all the users mentioned in a tweet

[source, ruby]
----
include::code/regexp_scan_twitter_users.rb[]
----

Output

----
The users are:
["@karthik_ak", "@ruby", "@yukihiro_matz"]
----

Notice the `string.scan(/@\w+/)` in the program above. It scans all words that is starting with an `@` symbol, collects them and returns them as an array, finally we display that array using `p users`.

=== Captures

We have seen how useful regular expressions could be. Now lets say we found a match with a  regular expression and we just want to capture a small part of it, say a user name in a email, or a month in some ones date of birth, how to do that?

We use round brackets for that and we call them captures. Below is a program that asks birthday of a person and extracts the month out of it.

[source, ruby]
----
include::code/regexp_capture.rb[]
----

Output

----
Enter Birthday (YYYY-MM-DD) :1982-11-22
You were born on month: 11
----

Notice this line `/\d{4}-(\d{2})-\d{2}/.match` date , here we check if the date matches the following: That is, it must have four digits `/\d{4}/`, then it must be followed by a hyphen `/\d{4}-/` then it must be followed by two digits  `/\d{4}-\d(2}/` and it must be followed a hyphen and another two digits `/\d{4}-\d{2}-\d{2}/`.

Now we need to capture just the month that's in the middle. Hence we put braces around it like shown `/\d{4}-(\d{2})-\d{2}/`, we stick this regexp in the program above, in this line

[source, ruby]
----
/\d{4}-(\d{2})-\d{2}/.match date
----

Now where this capture `(\d{2})` gets stored? It gets stored in a global variable `$1`, if there is another capture, it gets stored in another variable `$2`, `$3` and so on..... So we now know `$1` has the month and we use it in the following line to print out the result

[source, ruby]
----
puts "You were born on month: #{$1}"
----

In the coming example link:code/regexp_capture_1.rb[regexp_capture_1.rb], we try three captures where we want to capture Year, Month and Date in one go. Hence we use the following regexp `/(\d{4})-(\d{2})-(\d{2})/` . Type the program below and execute it.

[source, ruby]
----
include::code/regexp_capture_1.rb[]
----

Output

----
Enter Birthday (YYYY-DD-MM) :1997-12-67
Year: 1997
 Month: 12
 Date: 67
----

Here the first capture starting from left is stored in `$1`, the second is stored in `$2` and the third in `$3` and so on (if we had given more). If you are wondering what `$0` is, why don't you give a `puts $0` statement at the end of the program and see what happens?

In the next program, we have designed it to tolerate some errors in user input. The user may not always give 1990-04-17, he might give it as 1990 - 04- 17 or some thing like that that might have spaces around numbers. Type int the program and execute it

[source, ruby]
----
include::code/regexp_capture_2.rb[]
----

Output

----
Enter Birthday (YYYY-MM-DD) :1947- 07 - 14
Year: 1947
 Month: 07
 Date: 14
----

As you can see, the program finds month, date and year! If you note the regexp we are using `/\s*(\d{4})\s*-\s*(\d{2})\s*-\s*(\d{2})\s*/` we have padded digits with `\s*`, now whats that? Once again refer the regexp table <<Things to remember>>. `\s` means space and `*` means zero or more, so say `\s*\d{4}` means match in such a way that the regexp has 4 digits and is prepended with zero or more spaces and `\s*\d{4}\s*` match a 4 digit number which is prepended and followed by zero or more spaces. Hence no matter how much padding you give with space, it finds out the dates.

=== Nested Capture

Nested captures are capture within a capture. Type the code below and execute it in irb:

[source, ruby]
----
>> /(a(c(b)))/.match "This sting contains acb so it has match"
=> #<MatchData "acb" 1:"acb" 2:"cb" 3:"b">
>> $1
=> "acb"
>> $2
=> "cb"
>> $3
=> "b"
----

See the regexp `/(a(c(b)))/`, does it makes any sense to you? Well then how to read it? First remove all brackets and read it like `/acb/`.  `acb` is present in the string and hence its matched so we get the part of the output as shown below

[source, ruby]
----
=> #<MatchData "acb"....
----

Now apply the outer most bracket from left and we get a capture as shown `/(acb)/`, this matches and captures `acb` which appears as `1:"acb"` part in output as shown below

----
=> #<MatchData "acb" 1:"acb" 2:"cb" 3:"b">
----

This capture s stored in `$1` global variable. Now forget the outer bracket and move from left to the second pair of brackets and you get the following regexp `/a(cb)/`, this matches `acb` and captures `cb` in the string, this is caught in variable `$2` and is also shown in `2:"cb"` part of the matched data below

----
=> #<MatchData "acb" 1:"acb" 2:"cb" 3:"b">
----

In similar way the inner most bracket pair, forms this regexp /ac(b)/ and its captured in variable `$3` is showed in matched output `3:"b"` below

----
=> #<MatchData "acb" 1:"acb" 2:"cb" 3:"b">
----

=== MatchData class

So you have matched stuff and captured it. Well, if you have noticed in the irb, when ever you match a thing, a object gets returned. Everything is a object in Ruby, but this is not String type object, but a new type called `MatchData`. So lets play with it and see what it can do. So see the example below where we match a regexp `/abc/` with a string and we store it in a variable `m`

[source, ruby]
----
>> m = /abc/.match "This stuff has abc and something after it"
=> #<MatchData "abc">
----

To see what was matched, we give the command `m.match` and its throws an error as shown below!

[source, ruby]
----
>> m.match
NoMethodError: undefined method 'match' for #<MatchData "abc">
  from (irb):2
	from /home/webtoday/.rvm/rubies/ruby-1.9.3-p194/bin/irb:16:in '<main>'
----

So how to get the match? Well, it looks like the MatchData is an array where the first element is the matched piece of text so type `m[0]` to get the matched data as shown below

[source, ruby]
----
>> m[0]
=> "abc"
----

some times you might be interested what comes before and after a match. The `pre_match` function gets the piece of text that is prior to the match as shown below

[source, ruby]
----
>> m.pre_match
=> "This stuff has "
----

Like `pre_match`, `post_match` does the opposite, it gets the piece of text that comes after the match.

[source, ruby]
----
>> m.post_match
=> " and something after it"
----

If you want to see weather you have any captures, you can call the captures function as shown.

[source, ruby]
----
>> m.captures
=> []
----

Of course you have no captures this time, hence the captures function returns an empty array.

Well, talking about captures in MatchData object, take a look at the piece of code below. We have given a regexp with capture like `/((ab)c)/`. This regexp in the the string `"This stuff has abc and something after it"` will match `abc` and will capture `abc` and `ab` (if you have understood what capture is in the previous sections). Well, how to to get captures in MatchData object, first let us match the regexp with string and store it variable m as shown below

[source, ruby]
----
>> m = /((ab)c)/.match "This stuff has abc and something after it"
=> #<MatchData "abc" 1:"abc" 2:"ab">
----

Now to see captures, use the capture function on MatchData object as shown below

[source, ruby]
----
>> m.captures
=> ["abc", "ab"]
----

So you get captures as Array which can be treated as an Array object. You can get the captures directly from MatchData too as shown below, the second element onward in the MatchData array stores the captures which can be accessed by `m[1]`, `m[2]` …... `m[n]` as shown below

[source, ruby]
----
>> m[1]
=> "abc"
>> m[2]
=> "ab"
----

Well, I have told that `m` belongs to MatchData class, and haven't offered proof yet. Here is it

[source, ruby]
----
>> m.class
=> MatchData
----

=== Anchors and Assertions

==== Anchors

Anchors are reference points in Ruby. Lets say that we want to check if a line immediately begins with a =beginfootnote:[=begin represents start of block comment in Ruby, but it must start at the line beginning], then I can check it with a regexp like this `/^=begin/`, where the `^` sign is a anchor that represents beginning of the line:

[source, ruby]
----
/^=begin/.match "=begin"
=> #<MatchData "=begin">
----

Lets say like we have multiple line string and we want to extract a line (the first one). So the content of the first line could be any thing, so we get a regexp as `/.+/`, now it must be between beginning of line `^` and end of line `$`, so we get a regexp as shown `/^.+$/`, this regexp will match anything that's between line anchors. An example is shown below.

[source, ruby]
----
>> /^.+$/.match "Boom \n Thata"
=> #<MatchData "Boom ">
----

In the above example, note that `\n` splits the string into two lines as `\n` stands for newline character. So the regexp faithfully matches the first line content, that is `“Boom ”`.

The next type of Anchors we have are `\A` that stands for start of a string and `\z` that stands for end of a string. In the example below, we check if the string starts with some thing by using the following regexp `/\ASomething/`

[source, ruby]
----
>> /\ASomething/.match "Something is better than nothing"
=> #<MatchData "Something">
----

And we get a match. In the example below we get a nil match because the string does not start with Something.

[source, ruby]
----
>> /\ASomething/.match "Everybody says Something is better than nothing"
=> nil
----

Now lets check if nothing is followed by end of string, hence we form a regexp as shown `/nothing\z/`

[source, ruby]
----
>> /nothing\z/.match "Everybody says Something is better than nothing"
=> #<MatchData "nothing">
----

As expected we get a match for nothing. One should note that anchors will not be reflected in match data, anchor is not a character, but a symbolic representation of position. So if you are expecting a match for `nothing\z`, forget it.

[source, ruby]
----
>> /nothing\z/.match "Everybody says Something is better than nothing\n"
=> nil
----

Look at the example above, the `\z` matches a string without a line ending(`\n`) character. If you want to check for line endings, you must use capital `Z` like the example shown below

[source, ruby]
----
>> /nothing\Z/.match "Everybody says Something is better than nothing\n"
=> #<MatchData "nothing">
----

so it matches!

In the example below, we match all the stuff that's in a string with \n as its ending.

[source, ruby]
----
>> /\A.+\Z/.match "Everybody says Something is better than nothing\n"
=> #<MatchData "Everybody says Something is better than nothing">
----

==== Assertions

Lets say that you are searching for this man David Copperfield. You have a huge directory of names and you want to match his name. We can do those kind of matches using assertionsfootnote:[If you are thinking that this can be searched in much simple way, then you are right, but for the time being don't think too much. Your brain might overheat and burn.]. So lets say you want to search something that comes before Copperfield, for that we use look ahead assertions. Take the example below

[source, ruby]
----
>> /\w+\s+(?=Copperfield)/.match "David Copperfield"
=> #<MatchData "David ">
----

Look at the `(?=Copperfield)`, that is its looking forward for something, this time its `Copperfield`. Want to become rich soon? Then search for `(?=Goldfield`) and want some good music, search for (?=Oldfield)footnote:[Mike Oldfield is my favorite musician].

Here is the thing, what ever you give between `(?=` and `)` will be look forward and if there is something before it, it will get matched. So there is David before Copperfield, hence it was matched. Note that I had given `\w+\s+` which means that I want to match a regexp of one or more letters, followed by one or more spaced that precedes before Copperfield. So here we have another example, that gives a positive match:

[source, ruby]
----
>> /\w+\s+(?=Copperfield)/.match "Joan Copperfield"
=> #<MatchData "Joan ">
----

Lets say that we want to match all those names which does not end with Copperfield, we will use look ahead, negative assertion. For this we put Copperfield in between `(?!` and `)`, so in the following example it will return a negative match

[source, ruby]
----
>> /\w+\s+(?!Copperfield)/.match "Joan Copperfield"
=> nil
----

But in the next example it will return a positive match, because Joan is not before Copperfield

[source, ruby]
----
>> /\w+\s+(?!Copperfield)/.match "Joan Kansamy"
=> #<MatchData "Joan ">
----

We have seen look forward assertion. Now lets look at look backward assertion. Lets say that we want to match last name of person who's first name is David. Then we can look backwards from last name and see if its David. Checkout the code below

[source, ruby]
----
>> /(?<=David)\s+\w+/.match "Joan Kansamy"
=> nil
----

See the code above. We have put David between `(?<=` and `)`, so that's how you specify look back assertion. The above code returns nil because we have no David in it.

[source, ruby]
----
>> /(?<=David)\s+\w+/.match "David Munsamy"
=> #<MatchData " Munsamy">
----

The above example matches `“ Munsamy”`footnote:[Notice the space too has been matched.] because we have David before the pattern `\s+\w+`

Same way like we had negative look forward, why can't we have it in look backwards? Just replace `=` with a `!` and you will get a negative look backward assertion. So the example below will not match because you have David in front of Munsamy.

[source, ruby]
----
>> /(?<!David)\s+\w+/.match "David Munsamy"
=> nil
----

Now take this example below, we will get a match because there is no David in front of the first `\s+\w+`, that is in the example below it is a space followed by `“in”`.

[source, ruby]
----
>> /(?<!David)\s+\w+/.match "All in all Munsamy"
=> #<MatchData " in">
----

=== Ignoring Cases

Okay, lets say what the difference between these emails mindaslab@protonmail.com and MINDASLAB@PROTONMAIL.COM, nothing, both address delivers mail to me, so if at all we are scanning a string for a particular email, we would like to ignore cases. So consider the example below

[source, ruby]
----
>> /abc/i.match "There is ABC in string"
=> #<MatchData "ABC">
----

In the above example we have a regexp `/abc/` but it matches ABC in the given string. If you have noticed the, you may seen that I have put an `i` after the regexp, that `i` stands for ignore case. Well see the example below, the `i` ruthlessly matches anything and does not care about cases.

[source, ruby]
----
>> /abc/i.match "There is AbC in string"
=> #<MatchData "AbC">
----

=== Ignore Spaces

`x` just like `i` should be use at the rear of regexp. It ignores white spaces in regexp and matches the string. See example below

[source, ruby]
----
>> /ab c/x.match "There is abc in string"
=> #<MatchData "abc">
----

But this does not mean that it ignores white spaces in matched string, in the example below we have a regexp `/ab c/` (ab space c) but it does not match `ab c` (ab space c) in the string! That could be surprising. Which means when x is appended, it mean it removes all spaces from regexp.

[source, ruby]
----
>> /ab c/x.match "There is ab c in string"
=> nil
----

=== Dynamic Regexp

We may need to create Regexp dynamically, say I want to create a search query from the user data I obtained. Take a look at the program below, type it in a text editor and run it

[source, ruby]
----
include::code/regexp_dynamic.rb[]
----

Output

----
Enter search term: The
Nithya - The MBA. Oh NOOOOOO
Tat - The eskimo from Antartica
----

In the code we first declare an array called `Friends` ,that contains data about our friends as shown

[source, ruby]
----
Friends = [
  "Bharath - Looks like alien",
  "Nithya - The MBA. Oh NOOOOOO",
  "Tat - The eskimo from Antartica",
  "Kannan - Eats lot of bondas",
  "Karthik - Loves briyani"
]
----

So lets analyze the code. In the next two lines (shown below), I am getting search term and assigning it to a variable `term`

[source, ruby]
----
print "Enter search term: "
term = gets.chop
----

Next look at the following line carefully

[source, ruby]
----
regexp = Regexp.new term
----

Look at the part `Regexp.new term` , here is where all miracle happens. Now open irb and type the following

[source, ruby]
----
>> Regexp.new "something"
=> /something/
----

So as you see when you give a string to `Regexp.new` it converts it to Regexp. You can do pretty advanced stuff as shown below

[source, ruby]
----
>> r = Regexp.new "(\\d+)\\s+oranges"
=> /(\d+)\s+oranges/
----

So in `Regexp.new term` , we are converting the `term` to regular expression. Now all we need to do is to use this regular expression and pick up the strings that match it in the following code
`searched_friends = Friends.collect{|f| f if f.match regexp}.compact`

We print the array in the following code

[source, ruby]
----
puts searched_friends.join "\n"
----

Take a look at the simple calculator program I have written here https://raw.githubusercontent.com/mindaslab/ilrx/master/x/calculator.rb, it might be complex for newbies, so don't worry if you can't understand it.

