=== next

`break` , breaks out of loop and terminates it. `next` is some what different from `break` , instead of breaking from the loop, its a signal to continue the loop without executing statements  that occurs after `next`. Here is a example for you to understand it

[source, ruby]
----
include::code/next.rb[]
----

Output

----
0 
1 
2 
3 
4 
5 
7 
8 
9
----

If you notice the output, you see that numbers from 0 to 9 are printed, but there is no 6. Notice the line `next if num == 6` in link:code/next.rb[next.rb] here if `num` is `6`, `next` is executed, in other words all lines after that in the `do` `end` block is skipped. Unlike `brake` , the loop is not terminated, but just the lines after `next` are skipped and the loop continues on.

