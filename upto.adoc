=== upto

`upto` counts some number upto some other number. Its like downto in reverse. Type in the program below and execute it

[source, ruby]
----
include::code/upto.rb[]
----

And here is how the output looks like

----
17, 18, 19, 20, 21, 22, 23,
----


