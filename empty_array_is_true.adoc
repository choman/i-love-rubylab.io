=== Empty array is true

There is another stuff that must be known by a Ruby dev. It regarding conditions and empty array. Fire up your irb and type these

[source, ruby]
----
>> puts "A" if []
A
=> nil
----

If you see, the statement prints A even if the array passed to the `if` statement is empty. This is kinda against theory of least 
surprise, but not to get surprised imagine this. There is a book rack, and there are books in it so if you give a statement like this

[source, ruby]
----
>> puts "Books Present" if ["treasure island", "I Love Ruby"]
Books Present
=> nil
----

It does print Books Present as expected. But in this thing

[source, ruby]
----
>> puts "Books Present" if []
Books Present
=> nil
----

It still prints Books Present. Thats because even though the book rack is empty, there is a rack which is still an object. So there is some thing thats not `nil`. So its `true`. To make sure this is how it works take a look at the code below

[source, ruby]
----
>> nil.class
=> NilClass
>> [].class
=> Array
----

When we query whats the class of `nil`, it says its `NilClass` which is actually a empty thing. But when we query the class of an empty array its still an `Array`, which is not `nil` or `false` and hence its `true`. To check for empty array that must do as shown

[source, ruby]
----
>> puts "A" unless [].empty?
=> nil
>> puts "A" if [].first
=> nil
----

In the first one `[].empty?` returns `true`, but since its in `unles`s it would fail to execute the statement dependent on it.

If you see the second one we use `[].first` , this returns `nil`. Try it in irb

[source, ruby]
----
>> [].first
=> nil
----

So this could also be used to check emptiness of an array. Or is it so..... ?

[source, ruby]
----
>> a= [ nil, 1, 2, nil]
=> [nil, 1, 2, nil]
>> puts "a is empty" unless a.first
=> a is empty
>> puts "a is not empty" if a.first
=> nil
>> puts "a is not empty" unless a.empty?
a is not empty
=> nil
----

No think about weather or not to use `first` to check emptyness?

