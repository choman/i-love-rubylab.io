== Template Pattern

In template pattern, a class provides a base template, this base template is used by other classes for their intended purpose.

Lets see an example, look at the code shown below. We have a class called `News`, this news can be delivered via various mechanisms like text (say SMS), via the web in HTML format, or using json, or using XML.

So to deliver, the class `News` implements a basic template. It has method called `print` which prints output of three methods namely `header`, `body` and `footer`. That is, it defines a template saying that when news is to be delivered it must have a header, followed by the body of the news, then followed by the news footer.

Now type and run the program link:code/design_patterns/template.rb[template.rb] below.


[source, ruby, linenums]
----
include::code/design_patterns/template.rb[]
----

Output

----

    *************************
    *      TODAYS NEWS      *
    *************************


    Good Morning!
    =========================
    Nice weather today


    *************************
    *        GOODBYE!       *
    *************************

----

Now look at the class `PlainText`, it inherits from `News`, thus it must implement the pattern defined by `News`. So all it (`PlainText`) needs to do is to define the three methods namely `header`, `body` and `footer`. An so it does.

Now to print a news in plain text format all we need to do is to initialize a instance of `PlainText` class, and call `print` on it. Its done by the following piece of code

[source, ruby]
----
PlainText.new(
  "Good Morning!",
  "Nice weather today"
).print
----

So if you see, the template pattern defines a base template or structure, thus bringing about clarity and structure to class that want's to extend it. This may also reduce the coding needed to be done in the derived class.

=== Exercise

Why don't you modify the template pattern code so that we get out a HTML formatted text like this

[source, html]
----
<html>
  <head>
    <title>Today's News</title>
  </head>
  <body>
    <h1>Good Morning!</h1>
    <p>Nice weather today</p>
  </body>
</html>
----

