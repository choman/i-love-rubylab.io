# adapter_2.rb

require_relative "animal.rb"

class Animal
  module Adapter
    module Owl
      def self.speak
        puts "hoo!"
      end
    end
  end
end

animal = Animal.new
animal.adapter = :owl
animal.speak
