# factory.rb

class Shape
  def draw
    puts "In instance of #{self.class}"
  end
end

class Square < Shape
end

class Circle < Shape
end

class ShapeFactory
  def get_shape type
     case type
     when :square then Square.new
     when :circle then Circle.new
     end
  end
end

shape_factory = ShapeFactory.new
square = shape_factory.get_shape :square
circle = shape_factory.get_shape :circle

square.draw
circle.draw
