# composite.rb

require_relative "node.rb"

n1 = Node.new "n1"
n2 = Node.new "n2"
n3 = Node.new "n3"

n1.add_child n2
n1.add_child n3

puts "children of #{n1} are:"
for node in n1.children
  puts node
end

puts
puts "Parent of #{n3} is #{n3.parent}"
