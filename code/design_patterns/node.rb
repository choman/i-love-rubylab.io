class Node
  attr_accessor :name, :parent, :children

  def initialize name = "node"
    @name = name
    @parent = nil
    @children = []
  end

  def to_s
    "<Node: #{@name}, id: #{self.object_id}>"
  end

  def add_child node
    children << node
    node.parent = self
  end
end
