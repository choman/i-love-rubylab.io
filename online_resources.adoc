== Online Resources

Ruby has got an excellent online community of hackers who are ready to help almost any one who has any doubt about Ruby. They love the programming language and want others to love and experience it. Ruby is a great programming language that will put something good in your heart. Once you have learned it and start to interact with fellow hackers, you will naturally tend to help others. So do visit the websites recommended in this section. They might be of great use to you.

=== Ruby Website

URL: http://ruby-lang.org

Ruby website is a great place to start with Ruby. It provides you with the installers to install Ruby on your operating system. It has cool links like 'Try Ruby! in your browser', which lets you try out Ruby right from your web browser and a link called 'Ruby in Twenty Minutes' teaches you the basics of Ruby programming. Ruby is such a simple language that you just need 20 minutes to grasp it! Trust me it's true!

=== Reddit
Reddit has a very active Ruby community. You can find it here https://www.reddit.com/r/ruby/ . The  great thing about reddit is the stories are raked by fellow users, and whats the best bubbles up. If you are looking for what’s really trending in Ruby or any other tech topic, I find reddit being the best.

=== Ruby Flow

Ruby flow is a website which I use it for casual reading of whats happening in Ruby community. Its a very clean website where Rubyists can post what they think fellow Rubyists must know about Ruby. You can access it here http://www.rubyflow.com/

=== Twitter

URL: http://twitter.com

Twitter is a socializing website. Then why on Earth am I putting it here? Well lots of Ruby programmers use twitter, possibly because it was written with Ruby initially. To get the latest news about “Ruby Programming”, type it in the search bar and press search. You will get latest trending topics about Ruby language. Try searches like “Ruby language” and blah blah....

