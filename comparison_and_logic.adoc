== Comparison and Logic

include::logical_operators.adoc[]
include::true_not_true.adoc[]
include::triple_equal.adoc[]
include::if.adoc[]
include::if_else.adoc[]
include::elsif.adoc[]
include::if_then_else.adoc[]
include::unless.adoc[]
include::unless_else.adoc[]
include::case_when.adoc[]
include::case_when_checking_the_class_type.adoc[]
include::case_when_and_ranges.adoc[]
include::case_when_and_regular_expressions.adoc[]
include::case_when_and_lambdas.adoc[]
include::case_when_and_matcher_classes.adoc[]
include::ternary_operator.adoc[]
include::assigning_logic_statement_to_variables.adoc[]

