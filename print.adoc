= I Love Ruby
Karthikeyan A K <mindaslab@protonmail.com>
:toc:
:imagesdir: ./images
:source-highlighter: rouge
:doctype: book
:stem: asciimath
:icons: font

include::preface.adoc[]
include::ruby.adoc[]
include::copyright.adoc[]
include::getting_this_book.adoc[]
include::prerequisite.adoc[]

:sectnums:

include::installing_ruby.adoc[]
include::online_resources.adoc[]
include::getting_started.adoc[]
include::comparison_and_logic.adoc[]
include::loops.adoc[]
include::arrays.adoc[]
include::hashes_and_symbols.adoc[]
include::ranges.adoc[]
include::functions.adoc[]
include::variable_scope.adoc[]
include::classes_and_objects.adoc[]
include::safe_navigation.adoc[]
include::breaking_large_programs.adoc[]
include::struct_and_openstruct.adoc[]

include::rdoc.adoc[]

include::ruby_style_guides.adoc[]

include::modules_and_mixins.adoc[]
include::date_and_time.adoc[]
include::files.adoc[]
include::proc_lambdas_and_blocks.adoc[]
include::multi_threading.adoc[]
include::exception_handling.adoc[]
include::regular_expressions.adoc[]
include::gems.adoc[]
include::metaprogramming.adoc[]
include::benchmark.adoc[]
include::test_driven_development.adoc[]
include::design_patterns.adoc[]

