=== Constants

Unlike variables, some values must be constant, for example the radius of the Earth is constant, the speed of light is constant. In problems that deal with these kind of issues, or in situations where you are absolutely certain that some values wont change, you can use constants.

A constant can be thought as a variable who's value doesn't change. Constants in Ruby starts with a capital letter, it could then be followed by alphabets, numbers or underscore.  Lets now have a constant called `Pi` who value will be equal to mathematical stem:[pi], to do so just type the following in irb prompt

[source, ruby]
----
>> Pi = 3.1428 
=> 3.1428 
----

Having assigned the value of stem:[pi] to a constant named `Pi`, we will now try to find area a circle whose radius is 7 units, so lets use our faithful calculator the irb. We know that area of a circle is stem:[pi r^2], where stem:[r] is the circles radius. In your irb prompt we can do the calculation as follows

[source, ruby]
----
>> r =  7 
=> 7 
>> Pi * r ** 2 
=> 153.9972
----

So we find area of circle is roughly 153.9972 square units, which is very near to the exact value of 154 square units.

One can ask weather can we change value of constant? I don't say its impossible, but if we change ruby gives us warning that we are changing the value of a constant, after the warning the constant gets changed anyway.

[source, ruby]
----
>> Pi=5 
(irb):35: warning: already initialized constant Pi 
=> 5
----

In the above example I have re assigned the value of `Pi` to 5, as you can see in the second line, Ruby interpreter does throws out a warning that Pi is already initialized constant, but any way the value of Pi gets changed to 5. It is strongly discouraged not to change values of constants in professional programming.

