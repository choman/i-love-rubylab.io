== Files

Until now you have stored  data in variables in your program. Variable data gets lost when the program stops executing or when the computer is switched off or the program is removed from memory. If you want a persistent storage you must store it in files. When you store data in files, it stays there even if the program is removed from memory, and you can get the data back when its run again. This very book if you are reading it on computer, kindle or electronic reader is a file that's stored permanently on your computer or some other computer located on the internet. In this chapter we will see how we can create, manipulate and delete files using ruby program.

=== Storing output into files

Lets create a familiar Ruby program. Type the program below into a text editor

[source, ruby]
----
include::code/write_file.rb[]
----

While executing it, give command as shown

----
$ ruby write_file.rb > something.txt
----

Now goto the working directory in which the program was and you will see a file named link:code/something.txt[something.txt]. Open it and this is what you will see in it

----
Hello World!
Ruby can write into files
----

Well, this time its somewhat like a cheat. We haven't written into a file in our program, instead we have instructed the ruby interpreter to take the output that link:code/write_file.rb[write_file.rb] generates and put it in a file called link:code/something.txt[something.txt]. To do so we make use of  `>` (greater than sign).

=== Taking file as input

In the last example  we wrote our output to a file. Now lets take a file as input and lets process it. Write the code below in text editor and save it  as link:code/line_count.rb[line_count.rb].

[source, ruby]
----
include::code/line_count.rb[]
----

To execute it, we will give command as shown

----
$ ruby line_count.rb < something.txt
----

If you have guessed it right, we given link:code/something.txt[something.txt] as input to the program. We use the `<` (less than) sign to indicate to the program that we are giving a file as input.

The program when executed provides the following result

----
The file has 2 line(s)
----

Lets analyze the program so we know what happens. See this code `#{readlines.length}` in the program above. The `readlines` command takes in the file and reads all the lines and stores it in an array, each element of the array has a line. All we have to do is to get the length of an array which we can get by using the `length` function. So `readlines.length` gives the length as output which we embed it into a string hence we finish it off by writing the following statement

[source, ruby]
----
puts "The file has #{readlines.length} line(s)"
----

=== File copy – a kind of

Well, here is file copy program which might make some pundits argue weather if this program is a true file copy program or not. Type the program below into a text editor

[source, ruby]
----
include::code/file_copy.rb[]
----

And run it like this

----
$ ruby file_copy.rb < something.txt > everything.txt
----

Output

----
everthing.txt has got everything that something.txt has got. Just open it and see for yourself.
----

The trick is in the command line. Here we pass link:code/something.txt[something.txt] to the program link:code/file_copy.rb[file_copy.rb], now the program takes in link:code/something.txt[something.txt] and it reads the lines when it encounters the `readlines` command. The lines that are read from link:code/something.txt[something.txt] are stored in the form of an array. All we now have to do is to join the lines stored in array using `join` command, so we do it by adding `.join` to `readlines`, hence we get

[source, ruby]
----
readlines.join
----

Now we will print out the result, we do this by using a puts command, hence our program takes the following incarnation

[source, ruby]
----
puts readlines.join
----

While running the program we tell it to take input from link:code/something.txt[something.txt] and write the generated result to link:code/everything.txt[everything.txt] by giving the following command to Ruby interpreter

----
$ ruby file_copy.rb < something.txt > everything.txt
----

So we get the effect of copying without really writing a program for file copy.

=== Displaying a file

Lets now write a program that displays the content of a file. To do so we read all lines in a file, store it in an array. Next we take each and every element of an array and print it out. Type the program below and

[source, ruby]
----
include::code/display_file.rb[]
----

Execute it by typing the following

----
$ ruby display_file.rb < something.txt
----

This is what you will get as output

----
Hello World!
Ruby can write into files
----

So what we have done in this program.? Look at the this code block

[source, ruby]
----
readlines.each do |line|
  puts line
end
----

when ruby encounters `readlines`, it reads from the file passed to the program, extracts the lines and stores it in an array. With `.each` operator we extract a single line at a time and store it into a variable called `line` inside the `do` `end` block. We print out this line in the code block using `puts`. The `end` statement put an end to the code block says all is over.

Lets see another program. In this program we use a more cleaner approach. Type the program into a text editor and execute it

[source, ruby]
----
include::code/display_file_1.rb[]
----

Output

----
Hello World!
Ruby can write into files
----

Look at the single line in the program. We have a `puts` statement, that prints almost what ever is thrown at it. Here is the new thing that's been introduced. Look at the `File.open("something.txt")`, the `File.open` opens a file, but what file? We must give the name of the file to it. As a file name we pass `something.txt` in double quotesfootnote:[A name is a string right?]. The `File.open` opens it, the `.readlines` attached to it reads lines and stores it in an array. We throw the array to `puts` which prints it out. That's it!

=== Reading file line by line

Till the last section we have seen how to read a file in one go and pump its data out to the console. In this example we will see how to read a file line by line. Type in the example code given below and execute it

[source, ruby]
----
include::code/read_file_1.rb[]
----

Output

----
Hello World!
Ruby can write into files
----

The output looks as shown above. Look at the code `File.new("something.txt").each { |line| puts line }`.  In the code we open a file named link:code/something.txt[something.txt] using a `File.open` command which opens the file and stores the lines as array elements. All we need to do now is to extract each element in an array and print it out on our console which is accomplished by `.each { |line| puts line }`.

Instead of using `File.open`, one could use `File.new` to open a file. It will have the same result. A program using `File.new` has been written and is shown below, execute it and you will get the same result.

[source, ruby]
----
include::code/read_file_2.rb[]
----

Output

----
Hello World!
Ruby can write into files
----

=== Open and new – the difference

Seen from previous examples one might think that there isn't much difference between `File.open` and `File.new`, in fact there is a difference. Consider the program below, type it and execute it

[source, ruby]
----
include::code/file_open.rb[]
----

Output

----
Hello World!
----

The program above prints out the content present in link:code/something.txt[something.txt], the same thing is done by link:code/file_new.rb[file_new.rb] as shown below

[source, ruby]
----
include::code/file_new.rb[]
----

Output

----
Hello World!
----

OK so whats the difference? `File.new` returns a new file object or handle that can be stored in a variable. In the above program we store the file object into variable `f`. We can use this variable any where in the program to access and manipulate the file. Having done all needed with the file using the variable `f`, we finally close the file using `f.close`.

Lets write a program named link:code/file_open_error.rb[file_open_error.rb] as shown below

[source, ruby]
----
include::code/file_new.rb[]
----

Output

----
Hello World!

Reading file after File.open block is closed:
file_open_error.rb:8: undefined local variable or method `f' for main:Object (NameError)
----

See the highlighted code, we try to read the file content after we close the code block and it throws an error, this is because `File.open` loads into file handle into variable `f` inside the `do` `end` code block, after the block is closed you have no way to access the file.

Though the difference is minor, there is still a difference.

=== Defining our own line endings

Till now reading line by line means that the Ruby program when given a file name searches for it, loads it, then it scans the file, when it encounters a line ending character `'\n'` footnote:[The \n character will not be shown to the user and hence you wont be able to see it when you open the file with a text editor.] on the Linux system (its `\r\n` on windows) it recognizes the line has ended and hence packs the characters before it into an array element. What if we want to define our own line ending character? In English language full stop is considered as a line ending character. Why can't we say to the Ruby interpreter to mark end of the line at a full stop character? To do so lets create a simple text file named link:code/line_endings.txt[line_endings.txt] and put the following text in it

----
This is first line. This is second line. This
is the third. And fourth comes after third.
----

Lets write a Ruby program shown below in text editor, save it as link:code/line_endings.rb[line_endings.rb]

[source, ruby]
----
include::code/line_endings.rb[]
----

When executed, the program prints out the following output

----
This is first line.
 This is second line.
 This
is the third.
 And fourth comes after third.
----

See carefully link:code/line_endings.txt[line_endings.txt]. This is first line : _This is first line._ and This is second line : _This is second line._

Both are on the same line in link:code/line_endings.txt[line_endings.txt] but it gets printed out as two different lines when the program is executed. This is because the statement `File.open("line_endings.txt")` loads the entire content of the file into the memory, the `.each('.')` splits the content at every dot or full stop character ('.'), and puts the each chunk of split text into an array element. So the real hero here is the `each` function. In a similar way you can have any character that can define a line ending.

If you are writing a C compiler using Ruby, you might use the semicolon character ( ; ) as your line ending.

=== Reading byte by byte

Sometimes you want to read a file bytefootnote:[ http://wikipedia.org/byte] by byte instead of reading plain English in it. Why on earth we read a file byte by byte? Well, not all files have text in it. Files such as music files, videos and so on have raw data which only some programs can understand. If you are writing a music or video player or image viewer, you need to read the raw data and do something with it. So to read and display bytes of data we use `each_byte` function. Take a look at the code below. Type it and execute it

[source, ruby]
----
include::code/byte_by_byte.rb[]
----

When executed this is how the output will look like

----
72
101
108
108
111
32
87
111
.
.
some stuff is removed to save pages printed
.
.
105
108
101
115
10
----

In the above program we open the file named link:code/something.txt[something.txt] using `File.open`, all the contents gets loaded, now we access the content byte by byte using the `each_byte` function, we capture the bytes in variable called `byte` and print it out. Notice that in this program we have used curly brackets `{` and `}`, these can be used instead of `do` and `end` . I prefer `do` and `end` as they look more friendly.

=== Reading single character at a time

The program below reads character by character and prints it. We use a function called `each_char`. This `each_char` splits the input file character by character rather than line by line. This program and its output is given below.

[source, ruby]
----
include::code/char_by_char.rb[]
----

Output

----
H
e
l
l
o

W
o
r
l
d
!

R
u
b
y

c
a
n

w
r
i
t
e

i
n
t
o

f
i
l
e
s
----

=== Renaming files

Renaming a file is extremely easy in Ruby, all you have to do is to call the `rename` function in File class. The first argument will be the name of the file that needs to be renamed, the second one will be the new name. Its so simple you can try it out on the irb. Take a look at the source code of program link:code/rename.rb[rename.rb] given below. In it we rename a file called noname.txt to somename.txt. Before you run the program place a file called noname.txt on the working directory.

[source, ruby]
----
include::code/rename.rb[]
----

Output

----
The file noname.txt was renamed to somename.txt
----

=== Finding out position in a file

You might sometime need to find out your position within a file. To do so you can use the method `pos`. Lets see an example that explains us how to find our position in a file. Type and execute fie_position.rb

[source, ruby]
----
include::code/file_position.rb[]
----

Output

----
At the beginning f.pos = 0
After reading first line f.pos = 43
After reading second line f.pos = 69
----

Lets now walkthru the code and see how it works. First we open a file named link:code/god.txt[god.txt] in the line `f = File.open "god.txt"` next we check out whats the position using the statement `puts "At the beginning f.pos = #{f.pos}"`, note the `f.pos`, the `pos` method is used to get the position that we are in while we read or write a file. Initially when we open a file the position will be at zero and so we get the following output

----
At the beginning f.pos = 0
----

In the next line we read the first line of file using `f.gets`, since we have read the file like the reading pointers position should have changed4, so when we  print `f.pos` it must display some other number than zero. So the statement `puts "After reading first line f.pos = #{f.pos}"` produces the following result

----
After reading first line f.pos = 43
----

Just for the sake of educating more we read the second line using another `f.gets` now we print the new file position, now we find that the pointer points to position 69.

If you are wondering what link:code/god.txt[god.txt] has, here is it:

----
All things exists because it was created.
Then the creator exists.
Did man ever think how the creator exist?
If such a mighty creator can exist without creation,
then why can't this simple universe exist without
a creator
----

In the coming example we will see how to change our position within a file. Type the example below  (link:code/file_changing_position.rb[file_changing_position.rb]) and execute it

[source, ruby]
----
include::code/file_changing_position.rb[]
----

Output

----
Reading file with f.pos = 0
All things exists because it was created.
________________________________________
Reading file with f.pos = 12
xists because it was created.
Now f.pos = 43
----

Read the program carefully and notice the output. First we open the file link:code/god.txt[god.txt] and the variable `f` has its handle.

Next in line

[source, ruby]
----
puts f.gets
----

We are reading with file with `f.pos` at zero, that is we are reading from the start of file. As you can see the output for the first  `puts f.gets` we get the entire line `All things exists because it was created.`  gets printed. Notice the next line carefully, we now change our position within file to position 12 using the statement `f.pos = 12`, this means that our pointer is 12 bytes from the start. Now in the second puts `f.gets`, we get the output as exists because it was created. This shows us that we are able to change our position within a file in a successful way.

Some minds could think that there could be a possibility of negative file position where say if you want to read the last 20 bytes of file you can assign `f.pos = -20` and when giving f.gets it would get printed. Well, that's not possible with Ruby. If you want try out the example (file_negative_position.rb) and see weather it gives a proper result.

[source, ruby]
----
include::code/file_negative_position.rb[]
----

=== Writing into files

Till now we have seen how to read from files, we will now see how to write content into files. To learn how to write into files type the below example (link:code/write_file_1.rb[write_file_1.rb]) into the text editor and execute it

[source, ruby]
----
include::code/write_file_1.rb[]
----

After execution open the file link:code/god.txt[god.txt] and this is what you will see in it

----
All things exists because it was created.
Then the creator exists.
Did man ever think how the creator exist?
If such a mighty creator can exist without creation,
then why can't this simple universe exist without
a creator?
----

Lets walk thru the program and see how it works. First in the statement `File.open "god.txt", "w"`, we open a file named link:code/god.txt[god.txt]  for writing. We indicate that we are opening the file for writing by passing `“w”` as second argument. This second argument is called as a flag. Given below are list of flags that can be used for file operations.

|===
|Flag |What it says

|r
|The file is opened in read only mode. The file pointer is placed at the start of file.

| r+
| In r+ mode both reading and writing is allowed. The file pointer is placed at the start of the file

| w
| This means write only. If the file does not exist, a new file is created and data is written into it. If the file exists the previous content is replaced by new content

| w+
| In this mode both reading and writing is allowed. If the file does not exist, a new file is created. If it exists the old content is lost and new one s written.

| a
| This flag opens the file in append mode. Append mode is a special form of write mode in which the new content added is placed the end of old content5, by this way previous information isn't lost.

| a+
| Both reading and writing is allowed (i.e append mode plus reading and writing). Any newly added data is placed at the end of the file.

| b
| Binary file mode. In this mode files that have data other than text is read. For example opening a music or video file.

|===

Having opened a file in write mode we now have opened a `do` `end` block within which we capture the file handle in variable `f`. All we need to do is to write a string to the file.

We create a string using the following code

[source, ruby]
----
some_txt = <<END_OF_TXT
All things exists because it was created.
Then the creator exists.
Did man ever think how the creator exist?
If such a mighty creator can exist without creation,
then why can't this simple universe exist without
a creator?
END_OF_TXT
----

Now `some_txt` has got a string which we need to write it into the file. To write it into the file we use the following statement

[source, ruby]
----
f.puts some_txt
----

`gets` gets the file content, `puts` writes something into the file, so as an argument to the `puts` function we pass `some_txt`, the content held in it gets written into the file. The program reaches the end, the file is closed and that's it. When you open link:code/god.txt[god.txt] you can see what's written in it.

=== Appending content into files

Till now we have seen how to read from files and write content in it. Now lets see how to append content in it. While appending content into files, the old content stays on the new content is added at the bottom of the page.

To understand how this works type the program link:code/file_append.rb[file_append.rb] and execute it.

[source, ruby]
----
include::code/file_append.rb[]
----

When the program prompts you to enter some thing, type some thing like `It will be great if dinosaurs were still around` and press enter. Run this program a few times, type something, after you got bored from few run's open link:code/log_file.txt[log_file.txt] and see what it contains. When I opened mine, this is what I got:

----
Sat Mar 27 16:20:24 +0530 2010
This is my first log

Sat Mar 27 16:21:10 +0530 2010
This is my second log

Sat Mar 27 16:21:36 +0530 2010
This is my third log. Now I'm getting bored.
----

See how neatly your entries have been recorded along with time stamp. To understand how the program lets walk thru it.

The first line `puts "Enter text to append into file: "` , prints out `Enter text to append into file:` and the control goes on to the next line `text = gets` at which stage the program waits for you to enter something and press enter. When you do press enter, what you entered gets stored in variable `text`.

The next line `f = File.new("log_file.txt", "a")` is the crucial one and highlight of our program. In this line we open a file called link:code/log_file.txt[log_file.txt] in append mode. Notice that we pass `“a”` as the second argument to `File.new` which tells that we are opening it in append mode. In this mode the content that was previously stored in the file is not erased and/or over written, instead whats new being added is written at the end of the file.

Once having opened in append mode, all we need to do is to put content stored in variable text into the file. Since the file handle is stored in variable `f`, we could have completed the program by writing `f.puts text`, but I wished it would be better if we logged our data with time stamps, and I have left line breaks before and after each log so that it will be nice to read, so I have written the code `f.puts "\n"+Time.now.to_s+"\n"+text`.

That's it, the content we have written at the program prompt and along with the time stamp gets stored into the file. At the end of the program it would have been nice if we had closed the file using `f.close`, I haven't done it in this program, but it works.

=== Storing objects into files

Till now we have seen to read, write and append into files, whats we stored and read were pieces of text. Now we will see how to store objects or instance of classes into files.

==== Pstore

Pstore is a binary file format into which you can store almost anything. In the coming example we are going to  store few objects that belongs to the square class. First we will be writing a class for square and put it into a file called link:code/square_class.rb[square_class.rb]. If you feel lazy copy the content and below and put it into the file, if you are a active guy/gal type it all by yourself, finally you will end up with the same thing.

[source, ruby]
----
include::code/square_class.rb[]
----

Once the square class is ready, we will use it in two different places. The first one is coming right now. We create a program called link:code/pstore_write.rb[pstore_write.rb], type the content given below in it

[source, ruby]
----
include::code/pstore_write.rb[]
----

We will walk thru the program now. The first line `require './square_class.rb'` includes the code of the square class into the program, by doing so we can write code as though the square class code is typed into the same file, this reduces lot of typing and makes the code look neat.

In the next four lines shown below, we declare two squares `s1` and `s2`, we assign `s1` side length to be 4 units and that of `s2` to be 7.

[source, ruby]
----
s1 = Square.new
s1.side_length = 4
s2 = Square.new
s2.side_length = 7
----

In the next line `require 'pstore'` we include the code needed to read and write the pstore file format. We don't need to write that code as its already written for us, all we need to do is to type `require 'pstore'` and that will include the code.


Next we create pstore file using the command  `store = Pstore.new('my_squares')`. This creates a pstore file called `my_squares` and passes on the file handle to the variable named `store`, with this variable `store` we can read, manipulate the file `my_squares`. To start writing into the file we need to start a transaction which is accomplished by the following block of code

[source, ruby]
----
store.transaction do

end
----

Now we can do transactions with the pstore file within the `do` `end` block. Within the block we add the code that's highlighted below

[source, ruby]
----
store.transaction do
  store[:square] ||= Array.new
  store[:square] << s1
  store[:square] << s2
end
----

The first line creates a array named `store[:square]`, the `||=` means that if already a variable named `store[:square]` exists then there is no need to create that variable as its already there. If such a variable doesn't exist, then we need to create it. After creating an array we we add square objects / instance variables `s1` and `s2` into them using the following lines

[source, ruby]
----
store[:square] << s1
store[:square] << s2
----

Once done we close the transaction using the end command. Just view your working directory, you will be able to see a file named `my_squares` in it as shown in image below:

image::files-46cee.png[]

So now we have successfully written into the pstore file named `my_square`. All we need to do is read it and confirm what we have done is right. To read the data written into it we will write a program link:code/pstore_read.rb[pstore_read.rb].

Create a file named link:code/pstore_read.rb[pstore_read.rb] and store the program written below in it, execute and watch the output.

[source, ruby]
----
include::code/pstore_read.rb[]
----

Output

----
Area = 16
Perimeter = 16
===============================
Area = 49
Perimeter = 28
===============================
----

As you see the area and perimeter of the two squares are printed. If you feel I am tricking  you check for our self with a calculator. Well to understand what happens in link:code/pstore_write.rb[pstore_write.rb] lets walkthru the code. In the first two lines

[source, ruby]
----
require 'square_class.rb'
require 'pstore'
----

we include the code in link:code/square_class.rb[square_class.rb] and code for reading and writing pstore files into our program. Just like the previous example we open the pstore file `my_squares` and store the file handle into the variable named `store` in the following line

[source, ruby]
----
store = PStore.new('my_squares')
----

Now we create a array named `squares` in the following line

[source, ruby]
----
squares = []
----

With the `store` variable (which is the `my_squares` handle) we open a transaction as shown

[source, ruby]
----
store.transaction do
  squares = store[:square]
end
----

In the `transaction` as shown in the code above we transfer the objects in variable `store[:squares]` to the declared variable `squares` using `squares = store[:square]`, so by this time the variable `square` must contain the content of two square objects which we defines in previous example link:code/pstore_write.rb[pstore_write.rb]

Once we have taken out the values we can close the transaction using the `end` key word.

In the following code

[source, ruby]
----
squares.each do |square|
	puts "Area = #{square.area}"
	puts "Perimeter = #{square.perimeter}"
	puts "==============================="
end
----

we take each object in array squares and load it into variable called `square` and we print out the squares perimeter and area.

=== YAML

YAML stands for YAML ain't XML. YAML is a markup language in which we can store something like data contained in Ruby objects. Lets write a program in which we store the data of the square objects into YAML and retrieve it. Note that in this program we are not saving the output YAML data into a file, why so? Simply because I am lazy enough. Type the code yaml_write.rb into  text editor and execute it

[source, ruby]
----
include::code/yaml_write.rb[]
----

When executed, the program will produce the following output

----
---
- !ruby/object:Square
  side_length: 17
- !ruby/object:Square
  side_length: 34
----

Lets now walkthru the program. The first two lines

[source, ruby]
----
require 'yaml'
require 'square_class'
----

imports the code needed to read and write into YAML files. The next one loads the code in the link:code/square_calss.rb[square_calss.rb] so that you can program with square objects.

In the following lines

[source, ruby]
----
s = Square.new 17
s1 = Square.new 34
----

We declare two Square objects. One has edge or side length of 17 units and other has side length of 34 units. In the next line

[source, ruby]
----
squares = [s, s1]
----

We pack the objects s and s1 into an array called squares. In the following line

[source, ruby]
----
puts YAML::dump squares
----

we dump the formed array into YAML and print it onto the screen using puts statement.

Copy the stuff that comes in as output. It will be used to write the next program link:code/yaml_read.rb[yaml_read.rb], type the code link:code/yaml_read.rb[yaml_read.rb] that's shown below into the text editor and execute it

[source, ruby]
----
include::code/yaml_read.rb[]
----

Look at the output

----
Area = 289
Perimeter = 68
===============================
Area = 1156
Perimeter = 136
===============================
----

The first set of area and perimeter that's been displayed is of Square `s` and second set is of Square `s1`. Lets walkthru the code, understand what is happening. As usual these lines:

[source, ruby]
----
require 'yaml'
require './square_class'
----

imports the code needed for YAML and second one imports code in link:code/square_class.rb[square_class.rb] which enables us to deal with `Square` objects. Next we have a multi line string yaml

[source, ruby]
----
yaml = <<END
---
- !ruby/object:Square
  side_length: 17
- !ruby/object:Square
  side_length: 34
END
----

The content of yaml is enclosed between `<<END` and `END`, note that the content of yaml is the output of the previous program. Concentrate on this line

[source, ruby]
----
squares = YAML::load(yaml)
----

Its here all magic happens. Here the Ruby magically finds out from the YAML file that we are loading data stored in an array, this array consists of two objects of class `Square` and first one has side length 17 units and another of 34 units. So the `YAML::load` phrases it into array of Square's and stores it into variable `squares`.

In the following code:

[source, ruby]
----
squares.each do |square|
  puts "Area = #{square.area}"
  puts "Perimeter = #{square.perimeter}"
  puts "==============================="
end
----

We load each element of array into a variable `square` and print its area and perimeter.

